var songs = [];
var amps = [];
var vols = [];
var button;
var gradient = 1;
var numTracks = 4;

var volhistory = [];
for (var i = 0; i < numTracks; i++)
{
  volhistory[i] = new Array();
}


function toggleSong() {
  for (var i = 0; i < numTracks;i++)
  {
  if (songs[i].isPlaying()) {
    songs[i].pause();
  } else {
    songs[i].play();
  }
}
}

function preload() {
  songs[0] = loadSound('resources/audio.mp3');
  songs[1] = loadSound('resources/b_and_d.mp3');
  songs[2] = loadSound('resources/harmony.mp3');
  songs[3] = loadSound('resources/melodies.mp3');
}

function setup() {
  frameRate(60);
  createCanvas(screen.width, screen.height);
  angleMode(DEGREES);
  button = createButton('toggle');
  button.mousePressed(toggleSong);

  for (var i = 0; i < numTracks; i++)
  {
    amps[i] = new p5.Amplitude();
    amps[i].setInput(songs[i]);
  }
  toggleSong();

}

function draw() {

  var increase = .25;
  var cutoff = .01;
  background(0);
  translate(width / 2, height / 2);
  rotate(-90);
  noFill();

  for (var i = 0; i < numTracks; i++)
  {
      vols[i] = amps[i].getLevel();
      volhistory[i].push(vols[i]);
  }

  for(var i = 0; i < numTracks; i++)
  {
    if (vols[i] > cutoff)
      vols[i] += (1-vols[i])*increase;
    if (vols[i] > 1)
      vols[i] = 1;
  }

  for (var i = 0; i < numTracks; i++)
  {
    strokeWeight(vols[i] * 10);
    stroke(i/numTracks*255, vols[i]/numTracks*255, vols[i]*255)
    visualizer(volhistory[i], width/10);
    rotate(360/numTracks);
  }

}


function visualizer(a, q)
{
  var siz = 345;
  beginShape();
  for (var i = 0; i < siz; i++) {
    var r = map(a[i], 0, 1, 35, q+i*6);
    var x = r * cos(i);
    var y = r * sin(i);
    vertex(x, y);
  }
  endShape();

  if (a.length > siz) {
    a.splice(0, 1);
  }
}
