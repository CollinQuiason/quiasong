var song;
var fft;
var spectrum;
var allsound;

function preload()
{
  song = loadSound("resources/natural_philosophy.mp3");
}

function setup()
{
  createCanvas(screem.width, screen.height);
  //allsound = new p5.AudioIn();
  fft = new p5.FFT(0.8, 256);
  //allsound.start();
  song.play();
  fft.setInput(song);


}

function draw()
{
  background(0);
  spectrum = fft.analyze();
  visualize();
  fill( 200, 0 , 200);
  textFont("Comic Sans MS");
  textAlign(CENTER);
  textSize(100);
  text("Natural Philosophy - Left Hand Shake", width/2, height/3);



}

function visualize()
{
  var w = width/spectrum.length*1.7;

  for (var i = 0; i < spectrum.length; i++)
  {
    var amp = spectrum[i];
    var y = map(amp, 0, 512, height, 0);
    //fill(y/2, 0, y*y/255);
    fill(amp*amp/200+30, amp*amp/400, amp*amp / 200+30);
    rect(i * w, y, w+40, height-y);
  }
}
